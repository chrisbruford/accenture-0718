import { Directive, ElementRef, Input, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';

@Directive({
    selector: '[appFontSize]'
})
export class FontSizeDirective {

    @Input()
    appFontSize: string;

    constructor(private el: ElementRef) {

    }

    ngOnInit() {
        this.el.nativeElement.style.fontSize = this.appFontSize;
    }
}
