import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'bananas',
  pure: false
})
export class BananasPipe implements PipeTransform {

  transform(value: string, replace: string): any {
    return value.replace(replace,'bananas');
  }

}
