import { Directive, TemplateRef, ViewContainerRef, Input, ElementRef } from '@angular/core';

@Directive({
    selector: '[appLoggedIn]'
})
export class LoggedInDirective {
    accessLevel = 5;
    
    @Input()
    appLoggedIn;

    constructor(
        public template: TemplateRef<any>, 
        public vc: ViewContainerRef
    ) {
    }

    ngOnInit() {
        if (this.accessLevel >= this.appLoggedIn) {
            this.vc.createEmbeddedView(this.template);
        }
    }

}
