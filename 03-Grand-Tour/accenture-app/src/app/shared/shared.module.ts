import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FontSizeDirective } from './font-size.directive';
import { LoggedInDirective } from './logged-in.directive';
import { BannedNameDirective } from './validators/banned-name.directive';
import { BananasPipe } from './pipes/bananas.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [FontSizeDirective, LoggedInDirective, BannedNameDirective, BananasPipe],
  declarations: [FontSizeDirective, LoggedInDirective, BannedNameDirective, BananasPipe]
})
export class SharedModule { }
