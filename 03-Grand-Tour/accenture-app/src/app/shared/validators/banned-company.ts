import { AbstractControl, ValidatorFn } from "../../../../node_modules/@angular/forms";

export function bannedCompany(company: string): ValidatorFn {
    return function validate(control: AbstractControl): { [key: string]: any } {
        if (control.value === company) {
            return { bannedCompany: control.value }
        }
        return null
    }
}