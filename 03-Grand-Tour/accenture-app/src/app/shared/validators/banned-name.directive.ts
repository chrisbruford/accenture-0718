import { Directive, Input } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl } from '@angular/forms';

@Directive({
  selector: '[appBannedName]',
  providers: [{provide: NG_VALIDATORS, useExisting: BannedNameDirective, multi: true}]
})
export class BannedNameDirective implements Validator {

    @Input() appBannedName = "Chris";

  constructor() { }

  validate(control: AbstractControl): {[key:string]: any} {
      if (control.value === this.appBannedName) {
        return {bannedName: control.value}
      } else {
          return null
      }
  }

}
