import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Resolve, Router } from '@angular/router';
import { Observable, empty, throwError, observable, of } from 'rxjs';
import { User } from './user.model';
import { UserService } from './user.service';
import { map, catchError } from '../../../node_modules/rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class UserResolveGuard implements Resolve<User[]> {

    defaults = [
        {
            firstname: "Trev",
            surname: "Wilde",
            company: "Accenture"
        },
        {
            firstname: "Allen",
            surname: "Gregory",
            company: "Accenture"
        },
        {
            firstname: "Tom",
            surname: "Jewell",
            company: "Accenture"
        }
    ]

    constructor(
        public userService: UserService,
        private router: Router
    ) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<User[]> {
        return this.userService.getUsers().pipe(
            catchError(err => {
                return of(null);
            }),
            map(users => {
                if (users) {
                    console.log(users);
                    return users
                } else {
                    this.router.navigate(['/bananas']);
                    return null
                }
            })
        )
    }
}
