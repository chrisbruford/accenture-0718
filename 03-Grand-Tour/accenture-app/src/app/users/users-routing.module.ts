import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, Router, RouterModule } from '../../../node_modules/@angular/router';
import { UsersComponent } from './users.component';
import { AddUserComponent } from './add-user/add-user.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { AuthGuard } from '../core/auth.guard';
import { CanDeactivateGuard } from '../core/can-deactivate.guard';
import { UserResolveGuard } from './user-resolve.guard';

const routes: Routes = [
    {
        path: '', 
        component: UsersComponent, 
        canActivate: [AuthGuard],
        canActivateChild: [AuthGuard],
        resolve: {users: UserResolveGuard},
        children: [
            { path: 'adduser', component: AddUserComponent, canDeactivate: [CanDeactivateGuard]},
            { path: 'edituser/:firstname', component: EditUserComponent, outlet: 'edituseroutlet'}
        ]
    }
]

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes)
    ],
    exports: [RouterModule],
    declarations: []
})
export class UsersRoutingModule { }
