import { Component, OnInit, Input } from '@angular/core';
import { User } from '../user.model';
import { CanComponentDeactivate } from '../../core/can-deactivate.guard';

@Component({
    selector: 'app-add-user',
    templateUrl: './add-user.component.html',
    styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit, CanComponentDeactivate {

    @Input() users: User[];
    user = new User();
    isSubmitted = false;

    constructor() { }

    ngOnInit() {
    }

    doSubmit() {
        this.users.push(this.user);
        this.user = new User();
        this.isSubmitted = true;
    }

    canDeactivate() {
        if (this.isSubmitted) {
            return true
        } else {
            return false
        }
    }

}
