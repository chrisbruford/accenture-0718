import { Component, OnInit } from '@angular/core';
import { User } from '../user.model';
import { ActivatedRoute } from '../../../../node_modules/@angular/router';
import { UserService } from '../user.service';
import { switchMap } from '../../../../node_modules/rxjs/operators';

@Component({
    selector: 'app-edit-user',
    templateUrl: './edit-user.component.html',
    styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {

    user = new User();

    constructor(
        public route: ActivatedRoute,
        public userService: UserService
    ) { }

    ngOnInit() {
        // this.route.params.subscribe((params: {firstname: string})=>{
        //     this.userService.getUserByFirstname(params.firstname).subscribe(user=>this.user = user);
        // })
        this.route.params.pipe(
            switchMap((params: { firstname: string }) => {
                return this.userService.getUserByFirstname(params.firstname)
            }))
            .subscribe(user => this.user = user);
    }

}
