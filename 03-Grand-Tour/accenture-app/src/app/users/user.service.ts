import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Customer } from './customer.model';
import { Observable, empty, of, throwError } from 'rxjs';
import { catchError} from 'rxjs/operators'
import { UsersComponent } from './users.component';
import { User } from './user.model';

@Injectable({
    providedIn: 'root'
})
export class UserService {

    private users: User[] = [
        {
            firstname: "trev",
            surname: "wilde",
            company: "accenture"
        },
        {
            firstname: "aLLen",
            surname: "Gregory",
            company: "Accenture"
        },
        {
            firstname: "tom",
            surname: "Jewell",
            company: "accenture"
        }
    ];

    constructor(public http: HttpClient) {}

    getUsers(): Observable<User[]> {
        // return this.http.get<User[]>('https://jsonplaceholder.typicode.com/users')
        return of([...this.users]);
        // return of(null);
        // return throwError(new Error('woops!'))
    }

    getUserByFirstname(firstname: string): Observable<User> {
        return of(this.users.find(user=>user.firstname === firstname));
    }

    getAllByFirstname(firstname: string): Observable<User[]> {
        return of(this.users.filter(user=>user.firstname===firstname));
    }

    addUser(user: User): Observable<User> {
        // return this.http.post<Customer>('https://jsonplaceholder.typicode.com/users', customer)
        // .pipe(
        //     catchError(err=>{
        //         console.log(err);
        //         return empty();
        //     })
        // );

        this.users.push(Object.assign({},user));
        return of(user);
    }
}
