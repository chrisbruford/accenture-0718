import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { User } from '../user.model';
import { bannedCompany } from '../../shared/validators/banned-company';
import { UserService } from '../user.service';

@Component({
    selector: 'app-add-user-reactive',
    templateUrl: './add-user-reactive.component.html',
    styleUrls: ['./add-user-reactive.component.css']
})
export class AddUserReactiveComponent implements OnInit {
    addUserForm: FormGroup;
    // addUserForm = new FormGroup({
    //     firstname: new FormControl(),
    //     surname: new FormControl(),
    //     company: new FormControl()
    // })

    users: User[];

    constructor(public fb: FormBuilder, public userService: UserService) { }

    ngOnInit() {
        this.userService.getUsers().subscribe(users=>this.users=users);

        this.addUserForm = this.fb.group({
            firstname: [
                '',
                [
                    Validators.required,
                    Validators.minLength(3),

                ],
                []
            ],
            surname: [
                '',
                [
                    Validators.required,
                    Validators.minLength(3),
                    Validators.maxLength(30)],
                []
            ],
            company: [
                '',
                [
                    Validators.required,
                    bannedCompany('QA')
                ],
                []
            ],
            hobbies: this.fb.array([this.createHobby()])
        });
    }

    doSubmit() {
        let user = new User();
        user.firstname = this.addUserForm.get('firstname').value;
        user.surname = this.addUserForm.get('surname').value;
        user.company = this.addUserForm.get('company').value;
        let hobbies = this.addUserForm.get('hobbies') as FormArray;
        for (let control in hobbies.controls) {
            console.log(`Hobby: ${hobbies.controls[control].value}`);
        }
        this.users.push(user);
    }

    populateRachel() {
        this.addUserForm.setValue({
            firstname: 'Rachel',
            surname: 'Tulley',
            company: 'Accenture'
        });
    }

    setWilde() {
        this.addUserForm.patchValue({
            surname: 'Wilde'
        })
    }

    createHobby() {
        return new FormControl();
    }

    addHobby() {
        let hobbies = this.addUserForm.get('hobbies') as FormArray;
        hobbies.push(this.createHobby());
    }

}
