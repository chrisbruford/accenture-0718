export class Customer {
    id: string;
    name: string;
    username: string;
    email: string;
}