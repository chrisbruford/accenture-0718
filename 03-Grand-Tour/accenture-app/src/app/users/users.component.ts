import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { User } from './user.model';
import { UserService } from './user.service';
import { Customer } from './customer.model';
import { Router, ActivatedRoute } from '../../../node_modules/@angular/router';
import { Observable } from '../../../node_modules/rxjs';
import { map } from '../../../node_modules/rxjs/operators';

@Component({
    selector: 'app-users',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

    today = new Date();
    message = "Welcome to the Users Component";
    customers: Customer[];

    myClasses = {
        pink: true,
        bold: true
    };

    logoURL = "https://upload.wikimedia.org/wikipedia/commons/thumb/c/cd/Accenture.svg/2000px-Accenture.svg.png";
    
    user = new User();
    users: User[];

    @Output()
    userSelected = new EventEmitter();

    constructor(
        public userService: UserService,
        public router: Router,
        public route: ActivatedRoute
    ) {}

    ngOnInit() {
        // this.user.firstname = "Chris";
        // this.user.surname = "Bruford";
        // this.user.company = "QA";
        // this.userService.getUsers().subscribe(users=>{
        //     this.users = users;
        // })

        this.route.data.subscribe((data: {users: User[]})=>{
            this.users = data.users;
        });

        // this.users = this.route.data.pipe(
        //     map(data=>data.users)
        // )
    }

    addUser(evt: MouseEvent, user: User) {
        this.users.push(user);
        this.userService.addUser(user);
    }

    editUser(user: User) {
        this.user = Object.assign({},user);
        this.userSelected.emit(user);
        this.router.navigate(['./',{outlets: {edituseroutlet: ['edituser',user.firstname]}}], {relativeTo: this.route});
    }

    // getCustomers() {
    //     this.userService.getCustomers().subscribe(customers=>{
    //         this.customers = customers;
    //     });
    // }

    // addCustomer() {
    //     let customer = new Customer();
    //     customer.email = "chrisbruford@gmail.com";
    //     customer.name = "Chris Bruford";
    //     customer.username = "cbruford";
    //     customer.id = "1";
    //     this.userService.addCustomer(customer).subscribe(customer=>{
    //         console.log(customer);
    //     })
    // }

}
