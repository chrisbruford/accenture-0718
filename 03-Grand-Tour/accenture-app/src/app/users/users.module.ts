import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UsersComponent } from './users.component';
import { SharedModule } from '../shared/shared.module';
import { AddUserComponent } from './add-user/add-user.component';
import { AddUserReactiveComponent } from './add-user-reactive/add-user-reactive.component';
import { UserService } from './user.service';
import { UsersRoutingModule } from './users-routing.module';
import { EditUserComponent } from './edit-user/edit-user.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    UsersRoutingModule,
    SharedModule
  ],
  exports: [UsersComponent, AddUserComponent],
  declarations: [UsersComponent, AddUserComponent, AddUserReactiveComponent, EditUserComponent],
  //providers: [UserService]
  providers: [{provide: UserService, useClass: UserService}]
})
export class UsersModule { }
