import { Component } from '@angular/core';
import { User } from './users/user.model';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {

    selectedUser: User;

    constructor() { }

    title = 'Accenture';

    userSelected(user: User) {
        this.selectedUser = user;
    }
}
