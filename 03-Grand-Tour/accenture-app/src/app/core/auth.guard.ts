import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild } from '@angular/router';
import { Observable, of } from 'rxjs';
import { delay, retry, timeout, map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanActivateChild {

    constructor(private http: HttpClient) {

    }

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> | Promise<boolean> | boolean {

            // return new Promise((resolve,reject)=>{
            //     setTimeout(()=>{resolve(true)},3000);
            // });

            console.log('CanActivate check');

            //rxjs makes life easy.
            // return this.http.get('server/authenticate').pipe(
            //     retry(3),
            //     timeout(5000),
            //     map(res=>true)
            // )

            return of(true).pipe(
                delay(500)
            )
    }

    canActivateChild(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        console.log('CanActivateChild check');

        return of(true).pipe(
            delay(500)
        );
    }
}
