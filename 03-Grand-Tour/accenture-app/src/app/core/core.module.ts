import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '../../../node_modules/@angular/common/http';
import { NotFoundComponent } from './not-found/not-found.component';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule
  ],
  exports: [NotFoundComponent],
  declarations: [NotFoundComponent]
})
export class CoreModule { }
