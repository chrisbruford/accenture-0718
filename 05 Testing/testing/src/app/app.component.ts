import { Component } from '@angular/core';
import { UserService } from './core/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
    constructor(public userService: UserService) {}
  title = 'app';
  users: any = [];

  getUsers() {
    this.userService.getUsers().subscribe(users=>this.users = users);
  }
}
