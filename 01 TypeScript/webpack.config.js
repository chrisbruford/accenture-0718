const webpack = require('webpack');
const path = require('path');
// const HtmlWebpackPlugin = require('html-webpack-plugin');
// const ExtractTextPlugin = require('extract-text-webpack-plugin');
// const helpers = require('./helpers');

module.exports = {
  entry: {
    'app': './ts/app.ts'
  },

  resolve: {
    extensions: ['.ts', '.js']
  },

  output: {
      path: path.join(__dirname, './js')
  },

  module: {
    rules: [
      {
        test: /\.ts$/,
        loaders: [
          {
            loader: 'awesome-typescript-loader',
            // options: { configFileName: 'tsconfig.json' }
          }
        ]
      }
    ]
  }
};
