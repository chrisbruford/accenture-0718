export class Car {
    private speed = 0;

    constructor(public make: string, public model: string) {
        
    }

    startEngine() {
        console.log("Brrrroooom");
    }
}

let myCar = new Car("Ford","Fiesta");
console.log(myCar.make);