import { Car } from './Car';

export class SuperCar extends Car {
    v12 = true;

    constructor(public make, public model) {
        super(make, model);
    }
}

let mySuperCar = new SuperCar("McLaren","M1");