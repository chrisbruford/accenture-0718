import { SuperCar } from './SuperCar';

{
    // // let myNumber = 15;
// // let myString = 'My string';
// // let myBoolean = true;
// // let myObject = {};
// // let myNull = null;
// // let myUndefined = undefined;
// // // let mySymbole = Symbol();
// // // let myArray: number[] = [1,2,3];

// function sum(a: number,b: number): void {
//     console.log(a+b);
// }

// // let wotsits = sum('a',3);

// //TUPLE
// let myThings: [string, number, boolean];
// myThings = ['bananas', 12 , true];
// //myThings = [12, 'bananas' , true]; //no!

// //ENUM
// enum Color {Red,Green,Blue}
// enum Corrections {
//     camelCase = "PascalCase"
// }

// let myFavouriteColour: Color;
// myFavouriteColour = Color.Blue;

// let corrected: Corrections;
// corrected = Corrections.camelCase;

// //functions that never return have a return type of never
// function errorLogger(err: Error): never {
//     //fancy logging code
//     throw err
// }

// let x: any;
// x = "funky string";

// //either or
// console.log((<string>x).toUpperCase());
// console.log((x as string).toUpperCase());
}

console.log('All is well');
let mySuperCar = new SuperCar('McLaren', 'M1');
console.log(mySuperCar.v12);