import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { fromEvent } from 'rxjs/observable/fromEvent';
import { of } from 'rxjs/observable/of';
import { timer } from 'rxjs/observable/timer';
import { interval } from 'rxjs/observable/interval';
import { switchAll, switchMap, throttleTime, auditTime, map, filter, take, concat, merge } from 'rxjs/operators';

//manually creating an observable
// let counter: Observable<number> = Observable.create((observer: Observer<number>)=>{
//     let i = 0;
//     let interval = setInterval((()=>{
//         observer.next(i++);
//         if (i == 10) { 
//             clearInterval(interval);
//             observer.complete();
//         }
//     }),1000);
// });

// counter.subscribe(
//     (number)=>{ 
//         //logic for emissions
//         console.log(number); 
//     },
//     (err)=>{ 
//         //logic for error handling
//         console.error(err) 
//     },
//     ()=>{ 
//         //logic for completion
//         console.log('no more data'); 
//     }
// );

//filter
// let isOdd = fromEvent(document.querySelector('input[type="number"]'),'change');

// isOdd.pipe(
//     filter((evt: Event)=>parseInt((<HTMLInputElement>evt.target).value)%2 === 0)
// ).subscribe((evt: Event)=>console.log((<HTMLInputElement>evt.target).value));

// //map
// let hashtagify = fromEvent(document.querySelector('#hashtagify'),'click');

// hashtagify.pipe(
//     map(()=>{
//         let toHashtag = <HTMLInputElement>document.querySelector('#tohashtag');
//         return `#${toHashtag.value}`
//     })
// ).subscribe(output=>console.log(output));

// //switch
// let countdown = fromEvent(document.querySelector('#countdown'),'click');

// countdown.pipe(
//     map((evt: MouseEvent)=>timer(1000,1000)),
//     switchAll()
// ).subscribe(output=>console.log(output));

//CONCAT
//takes the first 5 emissions then completes
let firstObservable = interval(1000).pipe(take(5))
//takes the first 10 emissions then completes
let secondObservable =  interval(500).pipe(take(10)) 
//will deliver 0-4 in 1 second intervals and then 0-9 in 0.5 second intervals
firstObservable.pipe(
    concat(secondObservable)
).subscribe((value: number)=>{
    //some logic???
    console.log(value)
});

// //MERGE
// //takes the first 5 emissions then completes
// let thirdObservable = interval(1000).pipe(take(5))
// //takes the first 10 emissions then completes
// let fourthObservable =  interval(500).pipe(take(10)) 
// //will deliver 0-4 in 1 second intervals at the same time as 0-9 in 0.5 second intervals
// thirdObservable.pipe(
//     merge(fourthObservable)
// ).subscribe((value: number)=>console.log(value));

// //switchmap and other operators
// let input = fromEvent(document.querySelector('input[type="search"]'),'keyup');

// input.pipe(
//     //takes the last emitted value every 500ms and emits that
//     auditTime(500),
//     switchMap((evt: KeyboardEvent)=>{
//         let searchTerm = (<HTMLInputElement>evt.target).value;
//         //make ajax request with current text (we simulate here)
//         console.log(`Sending AJAX request for search term ${searchTerm}`);
//         return getJSON(searchTerm);
//     })
// )
// .subscribe(evt=>console.log(evt));

// function getJSON(searchTerm: string) {
//     //returns an Observable that resolves in 3 seconds (simulating ultra slow connection to a service)
//     return Observable.create((observer: Observer<string>)=>{
//         setTimeout(()=>{
//             observer.next(`Here's your JSON data for ${searchTerm}`);
//         },3000)
//     })
// }